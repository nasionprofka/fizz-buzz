<?php
declare(strict_types=1);

namespace App;

use PHPUnit\Framework\TestCase;

final class FizzBuzzTest extends TestCase
{
    /**
     * @test
     */
    public function case1(): void
    {
        $fizzBuzz = new FizzBuzz;
        $actual = $fizzBuzz->translate(1);
        $this->assertSame("1", $actual);
    }

    /**
     * @test
     */
    public function case3(): void
    {
        $fizzBuzz = new FizzBuzz;
        $actual = $fizzBuzz->translate(3);
        $this->assertSame("Fizz", $actual);
    }

    /**
     * @test
     */
    public function case5(): void
    {
        $fizzBuzz = new FizzBuzz;
        $actual = $fizzBuzz->translate(5);
        $this->assertSame("Buzz", $actual);
    }

    /**
     * @test
     */
    public function case15(): void
    {
        $fizzBuzz = new FizzBuzz;
        $actual = $fizzBuzz->translate(15);
        $this->assertSame("FizzBuzz", $actual);
    }

    /**
     * @test
     */
    public function iterationTimes100(): void
    {
        $fizzBuzz = new FizzBuzz;
        $actual = $fizzBuzz->run();
        $this->assertEquals(100, count($actual));
    }

    /**
     * @test
     */
    public function checkIndex100(): void
    {
        $fizzBuzz = new FizzBuzz;
        $actual = $fizzBuzz->run();
        $this->assertFalse(array_key_exists(-1, $actual));
        $this->assertTrue(array_key_exists(0, $actual));
        $this->assertTrue(array_key_exists(50, $actual));
        $this->assertTrue(array_key_exists(99, $actual));
        $this->assertFalse(array_key_exists(100, $actual));
    }

    /**
     * @test
     */
    public function checkValue100(): void
    {
        $fizzBuzz = new FizzBuzz;
        $actual = $fizzBuzz->run();
        $this->assertSame("1",    $actual[  1 - 1]);
        $this->assertSame("2",    $actual[  2 - 1]);
        $this->assertSame("Fizz", $actual[  3 - 1]);
        $this->assertSame("4",    $actual[  4 - 1]);
        $this->assertSame("Buzz", $actual[  5 - 1]);

        $this->assertSame("29",       $actual[ 29 - 1]);
        $this->assertSame("FizzBuzz", $actual[ 30 - 1]);
        $this->assertSame("31",       $actual[ 31 - 1]);

        $this->assertSame("Fizz", $actual[ 99 - 1]);
        $this->assertSame("Buzz", $actual[100 - 1]);
    }
}