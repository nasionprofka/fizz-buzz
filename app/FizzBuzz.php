<?php
declare(strict_types=1);

namespace App;

final class FizzBuzz
{
    public function translate(int $num): string
    {
        if ( $num % 15 == 0 ) {
            return "FizzBuzz";
        }
        if ( $num % 3 == 0 ) {
            return "Fizz";
        }
        if ( $num % 5 == 0 ) {
            return "Buzz";
        }
        return strval($num);
    }

    public function run(): array
    {
        $output = array();
        for($i = 0; $i < 100; $i++) {
            $output[$i] = $this->translate($i + 1);
        }
        return $output;
    }
}