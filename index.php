<?php
declare(strict_types=1);

require __DIR__.'/vendor/autoload.php';
use App\FizzBuzz;

$fixBuzz = new FizzBuzz();
$items = $fixBuzz->run();

foreach ($items as $item) {
    echo $item."<br>";
}